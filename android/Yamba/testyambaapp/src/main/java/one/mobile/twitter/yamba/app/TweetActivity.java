package one.mobile.twitter.yamba.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.marakana.android.yamba.clientlib.YambaClient;
import com.marakana.android.yamba.clientlib.YambaClientException;


public class TweetActivity extends Activity {
    private Button tweetButton;
    private EditText tweetText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tweet);

        tweetButton = (Button) findViewById(R.id.tweet_button);
        tweetText = (EditText) findViewById(R.id.tweet_text);

        tweetButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = tweetText.getText().toString();

                new PostTask().execute(text);

                Log.d("Yamba", "onClicked with: "+text);
            }
        });
    }

    class PostTask extends AsyncTask<String, Void, Boolean> {
        private ProgressDialog dialog;

        @Override
        protected void onPreExecute(){
            dialog = ProgressDialog.show(TweetActivity.this, "Please Wait", "Posting tweet");
        }

        //worker thread
        @Override
        protected Boolean doInBackground(String... params) {
            YambaClient yambaClient = new YambaClient("student", "password");
            try {
                yambaClient.postStatus(params[0]);
                return true;
            } catch (YambaClientException e) {
                e.printStackTrace();
                return false;
            }
        }

        //UI thread
        @Override
        protected void onPostExecute(Boolean result) {
            dialog.dismiss();
            Toast.makeText(TweetActivity.this, result?"Success":"Failure", Toast.LENGTH_LONG).show();
        }


    }

}
