package com.sample.library.contact;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.test.AndroidTestCase;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by joelaguila on 4/30/14.
 */
public class PhoneNumberHelperTest extends AndroidTestCase {
    private static final String MOCK_NUMBER = "1-555-555-5555";

    @Override
    protected  void setUp() throws Exception{
        super.setUp();
        System.setProperty("dexmaker.dexcache", getContext().getCacheDir().getPath());
    }


    public void testGetPhoneNumber() {
        fail();
        final Context mockContext = mock(Context.class);
        final TelephonyManager mockTelephonyManager = mock(TelephonyManager.class);

        when(mockContext.getSystemService(Context.TELEPHONY_SERVICE))
                .thenReturn(mockTelephonyManager);
        when(mockTelephonyManager.getLine1Number()).thenReturn(MOCK_NUMBER);

        final String actualNumber = PhoneNumberHelper.getPhoneNumber(mockContext);
        final String expectedNumber = "1-555-555-5555";
        assertEquals(expectedNumber, actualNumber);
    }

    public void testHasPhoneNumberFalseWhenEmptyPhoneNumber(){
        final Context mockContext = mock(Context.class);
        final TelephonyManager mockTelephonyManager = mock(TelephonyManager.class);

        when(mockContext.getSystemService(Context.TELEPHONY_SERVICE))
                .thenReturn(mockTelephonyManager);
        when(mockTelephonyManager.getLine1Number()).thenReturn("");

        assertFalse(PhoneNumberHelper.hasPhoneNumber(mockContext));

    }
}
