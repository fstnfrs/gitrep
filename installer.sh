#!/bin/sh

for f in $1/*
do
  echo "Processing $f file..."
  adb install -r "$f"
done
