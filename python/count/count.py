#!/usr/bin/python
import sys

def Count(filename):
	f = open(filename, 'rU')
	from collections import Counter
	count = Counter(f.read().split())
	for item in count.items():
		print("{}\t{}".format(*item))
	f.close()

#pass filename on command line
def main():
	Count(sys.argv[1])

#calls main() function
if __name__ == '__main__':
	main()